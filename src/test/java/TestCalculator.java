import celsiusToFahrenheit.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCalculator {

    @Test
    public void testCalculator() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(68, calculator.convert("20"));
        Assertions.assertEquals(-4, calculator.convert("-20"));
        Assertions.assertEquals(32, calculator.convert("0"));
        Assertions.assertEquals(40.46, calculator.convert("4.7"));
    }
}
