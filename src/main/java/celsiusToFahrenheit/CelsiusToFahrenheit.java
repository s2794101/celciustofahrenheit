package celsiusToFahrenheit;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CelsiusToFahrenheit extends HttpServlet {
    private final Calculator calculator;

    public CelsiusToFahrenheit() {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String temperature = "";
        try {
            temperature = String.valueOf(calculator.convert(request.getParameter("temperature")));
        } catch (NumberFormatException e) {
            temperature = "Please enter a number!";
        }

        out.println("<DOCTYPE HTML\n +" +
                "<HTML>" +
                "<HEAD><TITLE>Temperature converter</TITLE></HEAD>\n" +
                "<BODY>" +
                "   <H1>Celsius to Fahrenheit converter!</H1>\n" +
                "   <P>Temperature in degrees Celsius: " +
                    request.getParameter("temperature") + "\n" +
                    "<P>In degrees Fahrenheit: " + temperature + "\n" +
                "</BODY>\n" +
                "</HTML>");
    }
}
