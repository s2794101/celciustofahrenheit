package celsiusToFahrenheit;

public class Calculator {
    /**
     * Converts a temperature in Celsius to a temperature in Fahrenheit
     * @param temperature the temperature in degrees Celsius
     * @return the temperature in degrees Fahrenheit
     */
    public double convert(String temperature) throws NumberFormatException {
        return Double.parseDouble(temperature) * 1.8 + 32;
    }
}
